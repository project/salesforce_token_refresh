<?php

namespace Drupal\salesforce_token_refresh;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\salesforce\SalesforceAuthProviderPluginManagerInterface;
use Drupal\salesforce\Rest\RestClient as SalesforceClient;


/**
 * Class SalesforceAPIService.
 */
class SalesforceAPIService {

  /**
   * Drupal\Core\Routing\CurrentRouteMatch definition.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRouteMatch;

  /**
   * Drupal\salesforce\Rest\RestClient.
   *
   * @var Drupal\salesforce\Rest\RestClient
   */
  protected $salesforceClient;

  /**
   * Drupal\Core\Session\AccountProxyInterface definition.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Config Factory Service Object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerFactory;

  /**
   * Active auth provider.
   *
   * @var \Drupal\salesforce\SalesforceAuthProviderInterface
   */
  protected $authProvider;

  /**
   * Constructs a new SalesforceAPIService object.
   */
  public function __construct(
    CurrentRouteMatch $current_route_match,
    AccountProxyInterface $current_user,
    ConfigFactoryInterface $config_factory,
    LoggerChannelFactory $logger_factory,
    SalesforceClient $salesforceClient,
    SalesforceAuthProviderPluginManagerInterface $authManager
  ) {
    $this->currentRouteMatch = $current_route_match;
    $this->currentUser = $current_user;
    $this->configFactory = $config_factory;
    $this->salesforceClient = $salesforceClient;
    $this->authProvider = $authManager->getProvider();
  }

  /**
   * Try to make a call to Salesforce if token expired then renew the token
   *
   * @return \Drupal\salesforce\Rest\RestResponse|\GuzzleHttp\Psr7\Response|mixed|\Psr\Http\Message\ResponseInterface
   */
  public function refreshToken() {
      return  $this->salesforceClient->apiCall('');
  }
}
